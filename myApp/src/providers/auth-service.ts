import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import * as firebase from 'firebase';
import { Router } from 'ionic'
import { HomePage } from './../../src/pages/home/home';
import {NavController} from "ionic-angular";


@Injectable()
export class AuthService {
  token: string;
  message: object;

  constructor (private navCtrl: NavController){}

  public register(credentials){
    firebase.auth().createUserWithEmailAndPassword(credentials.email, credentials.password)
      .then(
        data => console.log(data)
      ).catch(
        error => console.log(error)
    );
  }

  /**
   *
   * @param credentials
   */
  login(credentials){
    firebase.auth().signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(
      data => {
        console.log(data);
        firebase.auth().currentUser.getToken()
          .then(
            (token: string) => {
              this.token = token;
              this.navCtrl.push(HomePage);
            }
          )
      })
      .catch(
        error => {
          console.log(error);
        });
  }


  public logout() {

  }

  getToken(){
    firebase.auth().currentUser.getToken()
      .then(
        (token:string) => this.token = token
      );
    return this.token;
  }

  isLogged(){
    return this.token != null;
  }


}
