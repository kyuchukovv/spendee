import {Component, OnInit} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from './../../providers/auth-service';
/*
 Generated class for the Login page.
 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [AuthService]
})
export class LoginPage implements OnInit{
  email: any;
  password: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authService: AuthService,
  ) {}
  ngOnInit(){
  }

  /**
   *
   * @param form
   */
  onLogin(form: NgForm){
  let response = this.authService.login({
        email: form.value.email,
        password: form.value.password
      });
      
      this.navCtrl
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
