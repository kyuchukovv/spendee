import {Component, OnInit} from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';
import {LoginPage} from "../pages/login/login";

export const firebaseConfig = {
  apiKey: "AIzaSyA0W3rAogsh1r4UiaoItT6y0D06Gm8tJXY",
  authDomain: "moneysaverapp-2155c.firebaseapp.com",
  databaseURL: "https://moneysaverapp-2155c.firebaseio.com",
  storageBucket: "moneysaverapp-2155c.appspot.com",
  messagingSenderId: "130332171850"
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  ngOnInit(){
    firebase.initializeApp(firebaseConfig);
  }
  public rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
